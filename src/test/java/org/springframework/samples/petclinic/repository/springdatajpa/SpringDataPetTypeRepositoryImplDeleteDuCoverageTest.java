package org.springframework.samples.petclinic.repository.springdatajpa;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.model.Visit;

public class SpringDataPetTypeRepositoryImplDeleteDuCoverageTest {

	@Mock
	private Pet pet;
	
	@Mock
	private Visit visit;
	
	@Mock
	private EntityManager entityManager;
	
	@Mock
	private PetType petType;
	
	@Mock
	Query query;
	
	@InjectMocks
	private SpringDataPetTypeRepositoryImpl
			springDataPetTypeRepositoryImpl;
	

	private ArrayList<Pet> pets;
	private ArrayList<Visit> visits;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		Integer petTypeId = new Integer(13);
		Integer visitId = new Integer(14);
		pets = new ArrayList<Pet>();
		visits = new ArrayList<Visit>();
		
		when(petType.getId()).thenReturn(petTypeId);
		when(entityManager.createQuery(any(String.class))).thenReturn(query);
		when(query.getResultList()).thenReturn(pets);
		
		when(pet.getVisits()).thenReturn(visits);
		when(visit.getId()).thenReturn(visitId);
	}

	//	1, 3, 4, 5, 6, 7
	@Test
	public void entityManagerContainsWithTrueReturnValueIsGiven_InvokeDelete_CoverMainBodyWithoutForLoop() {
		when(entityManager.contains(petType)).thenReturn(true);
		springDataPetTypeRepositoryImpl.delete(petType);
	}

	// 1, 2, 4, 5, 6, 8, 9, 10, 11, 12, 10, 13, 14, 6, 8, 9, 10, 11, 12, 10, 13, 14, 6, 7
	@Test
	public void petTypeWithTwoPetsAndTwoVisitsIsGiven_InvokeDelete_CoverBothLoops() {
		pets.add(pet);
		pets.add(pet);
		visits.add(visit);
		visits.add(visit);
		when(entityManager.contains(petType)).thenReturn(false);
		springDataPetTypeRepositoryImpl.delete(petType);
	}

}
