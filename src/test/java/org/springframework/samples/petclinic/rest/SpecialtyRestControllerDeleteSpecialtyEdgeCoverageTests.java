package org.springframework.samples.petclinic.rest;


import org.springframework.dao.EmptyResultDataAccessException;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.samples.petclinic.model.Specialty;
import org.springframework.samples.petclinic.repository.*;
import org.springframework.samples.petclinic.repository.SpecialtyRepository;
import org.springframework.samples.petclinic.repository.jpa.JpaSpecialtyRepositoryImpl;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.samples.petclinic.service.ClinicServiceImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SpecialtyRestControllerDeleteSpecialtyEdgeCoverageTests {
	
	@Mock
	Specialty specialty;
	
	@Mock
	Query query;
	
	@Mock
	PetRepository petRepository;

	@Mock
	VetRepository vetRepository;

	@Mock
	OwnerRepository ownerRepository;

	@Mock
	VisitRepository visitRepository;

	@Mock
	PetTypeRepository petTypeRepository;
	
	@Mock
	EntityManager em;
	
	private int specialtyId = 13;
	
	@InjectMocks
	SpecialtyRepository specialtyRepository = spy(new JpaSpecialtyRepositoryImpl());
	
	@InjectMocks
	ClinicService clinicService = spy(new ClinicServiceImpl(petRepository, vetRepository,
				ownerRepository, visitRepository, specialtyRepository, petTypeRepository));
	
	@InjectMocks
	SpecialtyRestController specialtyRestController;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(em.createQuery(any(String.class))).thenReturn(query);
		when(em.createNativeQuery(any(String.class))).thenReturn(query);
	}
	
	@Test
	public void emContainsSpecialtyTrueIsGiven_DeleteSpecialtyInvoked_FirstPathCoverageExpected() {
		when(em.contains(specialty)).thenReturn(true);
		specialtyRestController.deleteSpecialty(specialtyId);
	}

	@Test
	public void emContainsSpecialtyFalseIsGiven_DeleteSpecialtyInvoked_FirstPathCoverageExpected() {
		when(em.contains(specialty)).thenReturn(false);
		specialtyRestController.deleteSpecialty(specialtyId);
	}
	
	@Test
	public void emContainsSpecialtyTrueemptyResultDataAccessExceptionSpecialtyIdIsGiven_DeleteSpecialtyInvoked_FirstPathCoverageExpected() {
		when(em.contains(specialty)).thenReturn(true);
		Throwable emptyResultDataAccessException = new EmptyResultDataAccessException(specialtyId);
		when(specialtyRepository.findById(specialtyId)).thenThrow(emptyResultDataAccessException);
		specialtyRestController.deleteSpecialty(specialtyId);
	}
	
	@Test
	public void emContainsSpecialtyFalseEmptyResultDataAccessExceptionSpecialtyIdIsGiven_DeleteSpecialtyInvoked_FirstPathCoverageExpected() {
		when(em.contains(specialty)).thenReturn(false);
		Throwable emptyResultDataAccessException = new EmptyResultDataAccessException(specialtyId);
		when(specialtyRepository.findById(specialtyId)).thenThrow(emptyResultDataAccessException);
		specialtyRestController.deleteSpecialty(specialtyId);
	}
	
	@Test
	public void specialtyNotNullAndEmContainsTrueIsGiven_DeleteSpecialtyInvoked_FirstPathCoverageExpected() {
		specialty = new Specialty();
		when(clinicService.findSpecialtyById(specialtyId)).thenReturn(specialty);
		when(em.contains(specialty)).thenReturn(true);
		
		specialtyRestController.deleteSpecialty(specialtyId);
	}

	@Test
	public void specialtyNotNullAndEmContainsFalseIsGiven_DeleteSpecialtyInvoked_FirstPathCoverageExpected() {
		specialty = new Specialty();
		when(clinicService.findSpecialtyById(specialtyId)).thenReturn(specialty);
		when(em.contains(specialty)).thenReturn(false);
		
		specialtyRestController.deleteSpecialty(specialtyId);
	}
}
