package org.springframework.samples.petclinic.rest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.samples.petclinic.model.Specialty;
import org.springframework.samples.petclinic.model.Vet;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.validation.BindingResult;

import antlr.collections.List;

import static org.mockito.Mockito.*;

public class VetRestControllerUpdateVetEdgeCoverageTests {
	@Mock
	Vet vet;
	@Mock
	BindingResult bindingResult;
	@Mock
	ClinicService clinicService;
	@Mock
	Specialty specialty;
	@InjectMocks
	VetRestController vetRestController;
	
	ArrayList<Specialty> specialties;
	
	int vetId;
	
	private void setSpecialtieswithOneItem() {
		specialties.add(specialty);
	}
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		specialties = new ArrayList<Specialty>();
	}
	
	@Test
	public void bindingResultWithErrorisGiven_InvokeUpdateVet_CoverFirstIfBodyExpected() {
		when(bindingResult.hasErrors()).thenReturn(true);
		vetRestController.updateVet(vetId, vet, bindingResult);
	}
	
	@Test
	public void bindingResultWithoutErrorAndNotNullVetAndNotExistVetIdisGiven_InvokeUpdateVet_CoverSecondIfBodyExpected() {
		when(bindingResult.hasErrors()).thenReturn(false);
		when(clinicService.findVetById(anyInt())).thenReturn(null);
		vetRestController.updateVet(vetId, vet, bindingResult);
	}
	
	@Test
	public void bindingResultWithoutErrorAndNotNullVetAndExistVetIdisGiven_InvokeUpdateVet_CoverSecondIfBodyExpected() {
		setSpecialtieswithOneItem();
		when(bindingResult.hasErrors()).thenReturn(false);
		when(clinicService.findVetById(anyInt())).thenReturn(vet);
		when(vet.getSpecialties()).thenReturn(specialties);
		vetRestController.updateVet(vetId, vet, bindingResult);
	}
	
	@Test
	public void bindingResultWithErrorAndNullVetIsGiven_InvokeUpdateVet_CoverAllTheConditionsExpected() {
		vet = null;
		when(bindingResult.hasErrors()).thenReturn(false);
		vetRestController.updateVet(vetId, vet, bindingResult);
	}
}
