package org.springframework.samples.petclinic.caching;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.internal.util.reflection.FieldSetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

@Configuration
@Profile("test")
@ComponentScan(value={"org.springframework.samples.petclinic.caching.SimpleCache"})
class SimpleCacheConfiguration {
	
	@Mock
	EntityManager entityManager;
	
	@Mock
	Map<EntityIdentity, Object> cache;
	
	public SimpleCacheConfiguration() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Bean
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Bean
	public Map<EntityIdentity, Object> cache() {
		return cache;
	}
	
	@Bean
	public SimpleCache getSimpleCache() {
		return new SimpleCache(getEntityManager());
	}
}

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(value="test")
@ContextConfiguration(classes = SimpleCacheConfiguration.class)
public class SimpleCacheTests {
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	Map<EntityIdentity, Object> cache;
	
	@Autowired
	SimpleCache simpleCache;
	
	@Captor
	ArgumentCaptor<Object> idCaptor;
	
	@Captor
	ArgumentCaptor<Class<Object>> clazzCaptor;
	
	@Captor
	ArgumentCaptor<EntityIdentity> entityIdentityCaptor;
	
	@Mock
	Object id;
	
	Class<?> clazz;
    
    private static Object getFinalField(Class<?> clazz, String fieldName, Object thisObject)
    		throws ReflectiveOperationException {

        Field field = clazz.getDeclaredField(fieldName);
        field.setAccessible(true);
        Field modifiers = Field.class.getDeclaredField("modifiers");
        modifiers.setAccessible(true);
        modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        return field.get(thisObject);
    }
    
    @Before
	public void init() throws ReflectiveOperationException {
		MockitoAnnotations.initMocks(this);
		FieldSetter.setField(simpleCache, simpleCache.getClass().getDeclaredField("cache"), cache);
	}
    
    @After
    public void tearDown() {
    	Mockito.reset(cache);
    	Mockito.reset(entityManager);
    }
	
	@Test
	public void twoDifferentObjectsCreated_InvokeEqualsMethod_NotEqualityExpected() {
		
		EntityIdentity firstEntityIdentity = new EntityIdentity(clazz, id);
		EntityIdentity secondEntityIdentity = new EntityIdentity(clazz, id);
		
		assertFalse(firstEntityIdentity.equals(secondEntityIdentity));
	}
	
	@Test
	public void twoDifferentObjectsCreated_InvokeHashCodeMethod_NotEqualityExpected() {
		
		EntityIdentity firstEntityIdentity = new EntityIdentity(clazz, id);
		EntityIdentity secondEntityIdentity = new EntityIdentity(clazz, id);
		
		assertNotEquals(firstEntityIdentity.hashCode(), secondEntityIdentity.hashCode());
	}
	
	@Test
	public void dummyObjectAndDummyClassCreated_InvokeEvictMethod_RemoveOfCacheInvocationExpected()
			throws ReflectiveOperationException {
		simpleCache.evict(clazz, id);
		verify(cache).remove(entityIdentityCaptor.capture());
		EntityIdentity entityIdentity = entityIdentityCaptor.getValue();
	
		assertEquals(id, getFinalField(EntityIdentity.class, "id", entityIdentity));
	}
	
	@Test
	public void dummyObjectAndDummyClassCreated_InvokeRetrieveMethod_ContainsKeyOfCacheInvocationExpected()
			throws ReflectiveOperationException {
		when(cache.containsKey(Mockito.any())).thenReturn(true);
		simpleCache.retrieve(clazz, id);
		verify(cache).containsKey(entityIdentityCaptor.capture());
		EntityIdentity entityIdentity = entityIdentityCaptor.getValue();
	
		assertEquals(id, getFinalField(EntityIdentity.class, "id", entityIdentity));
	}
	
	@Test
	public void existObjectAndDummyClassCreated_InvokeRetrieveMethod_GetOfCacheInvocationExpected()
			throws ReflectiveOperationException {
		when(cache.containsKey(Mockito.any())).thenReturn(true);
		simpleCache.retrieve(clazz, id);
		verify(cache).get(entityIdentityCaptor.capture());
		EntityIdentity entityIdentity = entityIdentityCaptor.getValue();
	
		assertEquals(id, getFinalField(EntityIdentity.class, "id", entityIdentity));
	}
	
	@Test
	public void existObjectAndDummyClassCreated_InvokeRetrieveMethod_FindOfEntityManagerInvocationNotExpected()
			throws ReflectiveOperationException {
		when(cache.containsKey(Mockito.any())).thenReturn(true);
		simpleCache.retrieve(clazz, id);
		verify(entityManager, never()).find(clazzCaptor.capture(), idCaptor.capture());
	}
	
	@Test
	public void existObjectAndDummyClassCreated_InvokeRetrieveMethod_PutOfCacheInvocationNotExpected()
			throws ReflectiveOperationException {
		when(cache.containsKey(Mockito.any())).thenReturn(true);
		simpleCache.retrieve(clazz, id);
		verify(cache, never()).put(entityIdentityCaptor.capture(), idCaptor.capture());
	}
	
	@Test
	public void missObjectAndDummyClassCreated_InvokeRetrieveMethod_GetOfCacheInvocationNotExpected()
			throws ReflectiveOperationException {
		when(cache.containsKey(Mockito.any())).thenReturn(false);
		simpleCache.retrieve(clazz, id);
		verify(cache, never()).get(entityIdentityCaptor.capture());
	}
	
	@Test
	public void missObjectAndDummyClassCreated_InvokeRetrieveMethod_FindOfEntityManagerInvocationExpected()
			throws ReflectiveOperationException {
		when(cache.containsKey(Mockito.any())).thenReturn(false);
		simpleCache.retrieve(clazz, id);
		verify(entityManager).find(clazzCaptor.capture(), idCaptor.capture());
		Object actualId = idCaptor.getValue();
		Class<Object> actualClazz = clazzCaptor.getValue();
	
		assertEquals(id, actualId);
		assertEquals(clazz, actualClazz);
	}
	
	@Test
	public void missObjectAndDummyClassCreated_InvokeRetrieveMethod_PutOfcacheInvocationExpected()
			throws ReflectiveOperationException {
		Class<Object> clazz = Object.class;
		when(cache.containsKey(Mockito.any())).thenReturn(false);
		when(entityManager.find(clazz, id)).thenReturn(id);
		simpleCache.retrieve(clazz, id);
		verify(cache).put(entityIdentityCaptor.capture(), idCaptor.capture());
		Object actualId = idCaptor.getValue();
		EntityIdentity actualEntityIdentity = entityIdentityCaptor.getValue();
	
		assertEquals(id, actualId);
		assertEquals(id, getFinalField(EntityIdentity.class, "id", actualEntityIdentity));
	}
}
