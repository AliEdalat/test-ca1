package org.springframework.samples.petclinic.service.userService;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserServiceImpl;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplEdgeCoverageTests {
	
	@Mock
	User user;
	@Mock
	UserRepository userRepository;
	@Mock
    Role firstIterationRole;
    @InjectMocks
    UserServiceImpl userService;
    
    HashSet<Role> roles;
    
    private void setRolesOfUserWithFirstRole() {
    	roles.add(firstIterationRole);
		when(user.getRoles()).thenReturn(roles);
    }
    
    private void setRole(Role role, String name, User user) {
    	when(role.getName()).thenReturn(name);
		when(role.getUser()).thenReturn(user);
    }
    
    private String roleStartedName = "ROLE_";
    
    private String notRoleStartedName = "R";
    
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		roles = new HashSet<Role>();
	}
	
	@Test(expected = Exception.class)
    public void nullRolesIsGiven_InvokeSaveUser_ExceptionExpected() throws Exception {
		when(user.getRoles()).thenReturn(null);
		userService.saveUser(user);
    }
	
	@Test
    public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_saveInvocationAfterOneIterationExpected() throws Exception {
		setRolesOfUserWithFirstRole();
		setRole(firstIterationRole, roleStartedName, user);
		userService.saveUser(user);
    }
	
	@Test
    public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_saveAndSetNameAndSetUserInvocationAfterOneIterationExpected() throws Exception {
		setRolesOfUserWithFirstRole();
		setRole(firstIterationRole, notRoleStartedName, null);
		userService.saveUser(user);
    }
}
