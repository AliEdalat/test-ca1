package org.springframework.samples.petclinic.service.userService;

import static org.mockito.Mockito.when;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserServiceImpl;

public class UserServiceImpDuPathCoverageTests {
	@Mock
	User user;
	@Mock
	UserRepository userRepository;
	@Mock
    Role firstIterationRole;
	@Mock
    Role secondIterationRole;
    @InjectMocks
    UserServiceImpl userService;
    
    HashSet<Role> roles;
    
    private void setRole(Role role, String name, User user) {
    	when(role.getName()).thenReturn(name);
		when(role.getUser()).thenReturn(user);
    }
    
    private void setRolesOfUserWithFirstRole() {
    	roles.add(firstIterationRole);
		when(user.getRoles()).thenReturn(roles);
    }
    
    private void setRolesOfUserWithAllRoles() {
    	roles.add(firstIterationRole);
		roles.add(secondIterationRole);
		when(user.getRoles()).thenReturn(roles);
    }
    
    private String roleStartedName = "ROLE_";
    
    private String notRoleStartedName = "R";
    
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		roles = new HashSet<Role>();
	}
	
	@Test(expected = Exception.class)
    public void nullRolesIsGiven_InvokeSaveUser_CoverExceptionPathExpected() throws Exception {
		when(user.getRoles()).thenReturn(null);
		userService.saveUser(user);
    }
	
	@Test
    public void roleStartedAndGetUserIstNullIsGiven_SaveUserInvocation_CoverSetUserBodyExpected() throws Exception {
		setRolesOfUserWithFirstRole();
		setRole(firstIterationRole, roleStartedName, null);
		userService.saveUser(user);
    }
	
	// roleStartedAndGetUserIsNotNullInFirstIterationAndNotRoleStartedAndGetUserIsNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetUserBodyInFirstIterationAndCoverSetNameAndSetUserBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, roleStartedName, user);
		setRole(secondIterationRole, notRoleStartedName, null);
		userService.saveUser(user);
    }
	
	// notRoleStartedAndGetUserIsNullInFirstIterationAndRoleStartedAndGetUserIsNotNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetUserAndSetNameBodyInFirstIterationAndCoverForWithoutBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, notRoleStartedName, null);
		setRole(secondIterationRole, roleStartedName, user);
		userService.saveUser(user);
    }
	
	// notRoleStartedAndGetUserIsNullInFirstIterationAndNotRoleStartedAndGetUserIsNotNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetUserAndSetNameBodyInFirstIterationAndCoverSetNameBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, notRoleStartedName, null);
		setRole(secondIterationRole, notRoleStartedName, user);
		userService.saveUser(user);
    }
}
