package org.springframework.samples.petclinic.service.userService;

import static org.mockito.Mockito.when;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserServiceImpl;

public class UserServiceImplPrimePathCoverage {
	@Mock
	User user;
	@Mock
	UserRepository userRepository;
	@Mock
    Role firstIterationRole;
	@Mock
    Role secondIterationRole;
    @InjectMocks
    UserServiceImpl userService;
    
    HashSet<Role> roles;
    
    private void setRole(Role role, String name, User user) {
    	when(role.getName()).thenReturn(name);
		when(role.getUser()).thenReturn(user);
    }
    
    private void setRolesOfUserWithFirstRole() {
    	roles.add(firstIterationRole);
		when(user.getRoles()).thenReturn(roles);
    }
    
    private void setRolesOfUserWithAllRoles() {
    	roles.add(firstIterationRole);
		roles.add(secondIterationRole);
		when(user.getRoles()).thenReturn(roles);
    }
    
    private String roleStartedName = "ROLE_";
    
    private String notRoleStartedName = "R";
    
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		roles = new HashSet<Role>();
	}
	
	@Test(expected = Exception.class)
    public void nullRolesIsGiven_InvokeSaveUser_CoverExceptionPathExpected() throws Exception {
		when(user.getRoles()).thenReturn(null);
		userService.saveUser(user);
    }
	
	@Test
    public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_CoverForWithoutIfBodyExpected() throws Exception {
		setRolesOfUserWithFirstRole();
		setRole(firstIterationRole, roleStartedName, user);
		userService.saveUser(user);
    }
	
	@Test
    public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_CoverRoleNameAndSetUserBodyExpected() throws Exception {
		setRolesOfUserWithFirstRole();
		setRole(firstIterationRole, notRoleStartedName, null);
		userService.saveUser(user);
    }
	
	@Test
    public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_CoverSetNameBodyExpected() throws Exception {
		setRolesOfUserWithFirstRole();
		setRole(firstIterationRole, notRoleStartedName, user);
		userService.saveUser(user);
    }
	
	// roleStartedAndGetUserIsNullInFirstIterationAndNotRoleStartedAndGetUserIsNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetUserBodyInFirstIterationAndCoverSetNameAndSetUserBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, roleStartedName, null);
		setRole(secondIterationRole, notRoleStartedName, null);
		userService.saveUser(user);
    }
	
	// notRoleStartedAndGetUserIsNullInFirstIterationAndNotRoleStartedAndGetUserIsNotNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetUserAndSetNameBodyInFirstIterationAndCoverSetNameBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, notRoleStartedName, null);
		setRole(secondIterationRole, notRoleStartedName, user);
		userService.saveUser(user);
    }
	
	// notRoleStartedAndGetUserIsNullInFirstIterationAndRoleStartedAndGetUserIsNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetUserAndSetNameBodyInFirstIterationAndCoverSetUserBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, notRoleStartedName, null);
		setRole(secondIterationRole, roleStartedName, null);
		userService.saveUser(user);
    }
	
	// notRoleStartedAndGetUserIsNotNullInFirstIterationAndRoleStartedAndGetUserIsNotNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetNameBodyInFirstIterationAndCoverForWithoutIfBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, notRoleStartedName, user);
		setRole(secondIterationRole, roleStartedName, user);
		userService.saveUser(user);
    }
	
	// notRoleStartedAndGetUserIsNotNullInFirstIterationAndNotRoleStartedAndGetUserIsNotNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetNameBodyInFirstIterationAndCoverSetNameBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, notRoleStartedName, user);
		setRole(secondIterationRole, notRoleStartedName, user);
		userService.saveUser(user);
    }
	
	// roleStartedAndGetUserIsNotNullInFirstIterationAndNotRoleStartedAndGetUserIsNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverForWithoutIfBodyInFirstIterationAndCoverSetNameAndSetUserBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, roleStartedName, user);
		setRole(secondIterationRole, notRoleStartedName, null);
		userService.saveUser(user);
    }
	
	// roleStartedAndGetUserIsNotNullInFirstIterationAndRoleStartedAndGetUserIsNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverForWithoutIfBodyInFirstIterationAndCoverSetUserBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, roleStartedName, user);
		setRole(secondIterationRole, roleStartedName, null);
		userService.saveUser(user);
    }
	
	// roleStartedAndGetUserIsNotNullInFirstIterationAndRoleStartedAndGetUserIsNotNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverForWithoutIfBodyInFirstIterationAndCoverForWithoutIfBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, roleStartedName, user);
		setRole(secondIterationRole, roleStartedName, user);
		userService.saveUser(user);
    }
	
	// roleStartedAndGetUserIsNullInFirstIterationAndRoleStartedAndGetUserIsNotNullInSecondIterationIsGiven_SaveUserInvocation_
	@Test
    public void CoverSetUserBodyInFirstIterationAndCoverForWithoutIfBodyInSecondIterationExpected() throws Exception {
		setRolesOfUserWithAllRoles();
		setRole(firstIterationRole, roleStartedName, null);
		setRole(secondIterationRole, roleStartedName, user);
		userService.saveUser(user);
    }
}
