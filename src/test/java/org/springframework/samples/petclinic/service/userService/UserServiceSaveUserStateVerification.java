package org.springframework.samples.petclinic.service.userService;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserServiceImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
@Profile("test")
@ComponentScan(value={"org.springframework.samples.petclinic.service.UserServiceImpl"})
class UserServiceConfiguration {
	
	@Bean
	public UserRepository getUserRepository() {
		return new UserRepositoryStub();
	}
	
	@Bean
	public UserServiceImpl getUserServiceImpl() {
		return new UserServiceImpl();
	}
	
	@Bean
	public Role getRole() {
		return new RoleMock();
	}
	
	@Bean
	public User getUser() {
		return new UserMock();
	}
}

class UserRepositoryStub implements UserRepository {
	
	private List<User> savedUsers = new ArrayList<User>();
	
	public void save(User user) throws DataAccessException {
		savedUsers.add(user);
	}
	
	public User lastSavedUser() {
		return savedUsers.get(savedUsers.size()-1);
	}
	
	public int getSavedLen() {
		return savedUsers.size();
	}
	
	public void clear() {
		savedUsers.clear();
	}
}

class RoleMock extends Role {
	
    private User user;
    private String name;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class UserMock extends User {

	private Set<Role> roles;

	public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(value="test")
@ContextConfiguration(classes = UserServiceConfiguration.class)
public class UserServiceSaveUserStateVerification {
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private Role role;
	@Autowired
	private User user;
	
	
	@Before
	public void setUp() {
		HashSet<Role> roles = new HashSet<Role>();
		roles.add(role);
		user.setRoles(roles);
	}

	@After
	public void tearDown() throws Exception {
		((UserRepositoryStub)userRepository).clear();
	}
	
	@Test
	public void roleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetUserOfOrderExpected()
			throws Exception {
		String givenName = "ROLE_123";
		
		role.setName(givenName);
		userServiceImpl.saveUser(user);

		assertEquals(user, role.getUser());
	}

	@Test
	public void roleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetNameOfOrderExpected()
			throws Exception {
		String givenName = "ROLE_123";
		String expectedName = "ROLE_123";
		
		role.setName(givenName);

		userServiceImpl.saveUser(user);
		
		assertTrue(expectedName.equals(role.getName()));
	}

	@Test
	public void roleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_StoreUserSizeOfOrderExpected()
			throws Exception {
		String givenName = "ROLE_123";
		
		role.setName(givenName);

		userServiceImpl.saveUser(user);

		assertEquals(1, ((UserRepositoryStub) userRepository).getSavedLen());
	}

	@Test
	public void roleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_StoreUserOfOrderExpected()
			throws Exception {
		String givenName = "ROLE_123";
		
		role.setName(givenName);

		userServiceImpl.saveUser(user);
		
		assertEquals(user, ((UserRepositoryStub) userRepository).lastSavedUser());
	}
	
	@Test
	public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SetUserExpected()
			throws Exception {
		String givenName = "ROLE_123";
		
		role.setName(givenName);
		role.setUser(user);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(user, role.getUser());
	}
	
	@Test
	public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SetNameExpected()
			throws Exception {
		String givenName = "ROLE_123";
		String expectedName = "ROLE_123";
		
		role.setName(givenName);		
		role.setUser(user);
		
		userServiceImpl.saveUser(user);
		
		assertTrue(expectedName.equals(role.getName()));
	}
	
	@Test
	public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_StoreUserSizeExpected()
			throws Exception {
		String givenName = "ROLE_123";
		
		role.setName(givenName);
		role.setUser(user);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(1, ((UserRepositoryStub) userRepository).getSavedLen());
	}
	
	@Test
	public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_StoreUserExpected()
			throws Exception {
		String givenName = "ROLE_123";
		
		role.setName(givenName);
		role.setUser(user);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(user, ((UserRepositoryStub) userRepository).lastSavedUser());
	}
	
	
	@Test
	public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetNameExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";
		String expectedName = "ROLE_NOT_ROLE_123";
		
		role.setName(givenName);
		
		userServiceImpl.saveUser(user);
		
		assertTrue(expectedName.equals(role.getName()));
	}

	@Test
	public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetUserExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";
		role.setName(givenName);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(user, role.getUser());
	}

	@Test
	public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_StoreUserSizeExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";
		role.setName(givenName);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(1, ((UserRepositoryStub) userRepository).getSavedLen());
	}

	@Test
	public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_StoreUserExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";
		
		role.setName(givenName);
		userServiceImpl.saveUser(user);
		
		assertEquals(user, ((UserRepositoryStub) userRepository).lastSavedUser());
	}
	
	@Test
	public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SetNameInvocationExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";
		String expectedName = "ROLE_NOT_ROLE_123";
		
		User expectedUser = new User();
		role.setName(givenName);
		role.setUser(expectedUser);
		
		userServiceImpl.saveUser(user);
		
		assertTrue(expectedName.equals(role.getName()));
	}
	
	@Test
	public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SetUserInvocationExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";
		
		User expectedUser = new User();
		role.setName(givenName);
		role.setUser(expectedUser);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(expectedUser, role.getUser());
	}

	@Test
	public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SaveUserSizeInvocationExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";
		
		User expectedUser = new User();
		role.setName(givenName);
		role.setUser(expectedUser);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(1, ((UserRepositoryStub) userRepository).getSavedLen());
	}
	
	@Test
	public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SaveUserInvocationExpected()
			throws Exception {
		
		String givenName = "NOT_ROLE_123";

		User expectedUser = new User();
		role.setName(givenName);
		role.setUser(expectedUser);
		
		userServiceImpl.saveUser(user);
		
		assertEquals(user, ((UserRepositoryStub) userRepository).lastSavedUser());
	}
}
