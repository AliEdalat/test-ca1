package org.springframework.samples.petclinic.service.userService;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserServiceImpl;

import static org.mockito.Mockito.*;

import java.util.HashSet;

import org.mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceSaveUserBehaviourVerification {
	@Mock
    UserRepository userRepository;

	@Mock
    User user;
    
    @Mock
    Role role;

    @InjectMocks
    UserServiceImpl userServiceImpl;
    
    @Captor
    ArgumentCaptor<String> argCaptor;
    
    @Captor
    ArgumentCaptor<User> userCaptor;
    
    @Captor
    ArgumentCaptor<User> saveUserCaptor;
    
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		
		HashSet<Role> roles = new HashSet<Role>();
		roles.add(role);
		
		when(user.getRoles()).thenReturn(roles);
		when(role.getUser()).thenReturn(null);
	}
	
	@Test(expected = Exception.class)
    public void nullRolesIsGiven_InvokeSaveUser_ExceptionExpected() throws Exception {
		when(user.getRoles()).thenReturn(null);
    	userServiceImpl.saveUser(user);
    }
	
	@Test(expected = Exception.class)
    public void emptyRolesIsGiven_InvokeSaveUser_ExceptionExpected() throws Exception {
		when(user.getRoles()).thenReturn(new HashSet<Role>());
    	userServiceImpl.saveUser(user);
    }

	@Test
	public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetNameInvocationExpected()
			throws Exception {
		String expectedName = "ROLE_NOT_ROLE_123";
		String givenName = "NOT_ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		
		userServiceImpl.saveUser(user);
		
		verify(role).setName(argCaptor.capture());
		assertTrue(argCaptor.getValue().equals(expectedName));
	}

	@Test
	public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetUserInvocationExpected()
			throws Exception {
		String givenName = "NOT_ROLE_123";
		when(role.getName()).thenReturn(givenName);
		
		userServiceImpl.saveUser(user);
		
		verify(role).setUser(userCaptor.capture());
		assertEquals(user, userCaptor.getValue());
	}

	@Test
	public void notRoleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SaveInvocationExpected()
			throws Exception {
		String givenName = "NOT_ROLE_123";
		when(role.getName()).thenReturn(givenName);
		
		userServiceImpl.saveUser(user);
		
		verify(userRepository).save(saveUserCaptor.capture());
		assertEquals(user, saveUserCaptor.getValue());
	}

	@Test
	public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SetNameInvocationExpected()
			throws Exception {
		String expectedName = "ROLE_NOT_ROLE_123";
		String givenName = "NOT_ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		when(role.getUser()).thenReturn(new User());

		userServiceImpl.saveUser(user);
		
		verify(role).setName(argCaptor.capture());
		assertTrue(argCaptor.getValue().equals(expectedName));
	}

	@Test
	public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SaveInvocationExpected()
			throws Exception {
		String givenName = "NOT_ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		when(role.getUser()).thenReturn(new User());
		
		userServiceImpl.saveUser(user);
		
		verify(role).setName(argCaptor.capture());
		verify(userRepository).save(userCaptor.capture());
		
		assertEquals(user, userCaptor.getValue());
	}

	
	@Test
	public void notRoleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_SetNameInvocationNotExpected()
			throws Exception {
		String givenName = "NOT_ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		when(role.getUser()).thenReturn(new User());
		
		userServiceImpl.saveUser(user);
		
		verify(role, never()).setUser(userCaptor.capture());
	}

	@Test
	public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_saveInvocationExpected()
			throws Exception {
		String givenName = "ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		when(role.getUser()).thenReturn(new User());
		
		userServiceImpl.saveUser(user);
		
		verify(userRepository).save(userCaptor.capture());
		
		assertEquals(user, userCaptor.getValue());
	}
	
	@Test
	public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_NotSetNameInvocationExpected()
			throws Exception {
		
		String givenName = "ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		when(role.getUser()).thenReturn(new User());
		
		userServiceImpl.saveUser(user);
		
		verify(role, never()).setName(argCaptor.capture());
	}

	@Test
	public void roleStartedAndGetUserIsNotNullIsGiven_SaveUserInvocation_NotSetUserInvocationExpected()
			throws Exception {
		
		String givenName = "ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		when(role.getUser()).thenReturn(new User());
		
		userServiceImpl.saveUser(user);
		
		verify(role, never()).setUser(userCaptor.capture());
	}
	
	
	@Test
	public void roleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetUserInvocationExpected()
			throws Exception {
		
		String givenName = "ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		
		userServiceImpl.saveUser(user);
		
		verify(role).setUser(userCaptor.capture());
		assertEquals(user, userCaptor.getValue());
	}

	
	@Test
	public void roleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SetNameNotInvocationExpected()
			throws Exception {
		
		String givenName = "ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		
		userServiceImpl.saveUser(user);
		
		verify(role, never()).setName(argCaptor.capture());
	}
	
	@Test
	public void roleStartedAndGetUserIsNullIsGiven_SaveUserInvocation_SaveInvocationExpected()
			throws Exception {
		
		String givenName = "ROLE_123";
		
		when(role.getName()).thenReturn(givenName);
		
		userServiceImpl.saveUser(user);
		
		verify(userRepository).save(userCaptor.capture());
		
		assertEquals(user, userCaptor.getValue());
	}
}
